//
//  ViewController.swift
//  todolist
//
//  Created by Eshcol Robert on 3/20/19.
//  Copyright © 2019 Eshcol Robert. All rights reserved.
//

import UIKit
import RealmSwift

class ToDoListViewController: UITableViewController {

    let realm = try! Realm()
    var todoItems: Results<Item>!
    let dataFilePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    
    var selectedCategory : Category? {
        didSet{
            loadItems()
        }
    }
    
    var textField = UITextField()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        print(dataFilePath!)
        
        // Do any additional setup after loading the view, typically from a nib.
        tableView.register(UINib(nibName: "ToDoItemCell", bundle: nil), forCellReuseIdentifier: "toDoItemCell")
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoItems?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "toDoItemCell", for: indexPath) as! ToDoItemCell
        
        if let item = todoItems?[indexPath.row] {
            cell.lblCell.text = item.task
            cell.accessoryType = item.done ? .checkmark : .none
        }
        else {
            cell.lblCell.text = "No Items added yet"
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let item = todoItems?[indexPath.row] {
            do {
                try realm.write {
                item.done = !item.done
//                    realm.delete(item)
                }
            } catch {
              print("Error saving done status, \(error)")
            }
        }
        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "Add an item", message: "", preferredStyle: .alert)
        
        alert.addTextField { (alertTextField) in
            
            alertTextField.placeholder = "Create an item"
            self.textField = alertTextField
        }
        
        let action = UIAlertAction(title: "Add Item", style: .default) { (action) in
            
            if let currentCategory = self.selectedCategory {
                do {
                    try self.realm.write {
                        let newItem = Item()
                        newItem.task = self.textField.text!
                        newItem.createDate = Date()
                        currentCategory.items.append(newItem)
                    }
                } catch {
                    print("Error saving context: \(error)")
                }
            }
            
            self.tableView.reloadData()
            
        }
        
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    func loadItems() {

        todoItems = selectedCategory?.items.sorted(byKeyPath: "createDate", ascending: false    )
        tableView.reloadData()
    }
    
//    func prepareRequest(reqAttr: String, reqPredicate: String, reqSource: String ) -> NSFetchRequest<Item> {
//
//        let request : NSFetchRequest<Item> = Item.fetchRequest()
//
//        request.predicate = NSPredicate(format: reqPredicate, reqSource)
//
//        request.sortDescriptors = [NSSortDescriptor(key: reqAttr, ascending: true)]
//
//        return request
//    }
}

//MARK: - Searchbar
extension ToDoListViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        todoItems = todoItems.filter("task CONTAINS %@", searchBar.text!).sorted(byKeyPath: "createDate", ascending: true)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if searchBar.text?.count == 0 {

            loadItems()

            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }


        }
    }
}
