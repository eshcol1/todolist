//
//  Item.swift
//  todolist
//
//  Created by Eshcol Robert on 3/25/19.
//  Copyright © 2019 Eshcol Robert. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
    @objc dynamic var task: String = ""
    @objc dynamic var done: Bool = false
    @objc dynamic var createDate: Date = Date()
    var parentCategory = LinkingObjects(fromType: Category.self, property: "items")
}
