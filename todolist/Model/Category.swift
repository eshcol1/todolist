//
//  Category.swift
//  todolist
//
//  Created by Eshcol Robert on 3/25/19.
//  Copyright © 2019 Eshcol Robert. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name: String = ""
    let items = List<Item>()
}
