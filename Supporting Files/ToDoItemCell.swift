//
//  ToDoItemCell.swift
//  todolist
//
//  Created by Eshcol Robert on 3/20/19.
//  Copyright © 2019 Eshcol Robert. All rights reserved.
//

import UIKit

class ToDoItemCell: UITableViewCell {

    @IBOutlet weak var lblCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
